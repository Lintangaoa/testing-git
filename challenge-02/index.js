//nilaiTertinggi
const nilaiTertinggi = (values) => {
    var tertinggi = Math.max.apply(null, values);

    return tertinggi;
}
console.log(nilaiTertinggi([50, 77, 31, 56, 88]));
//nilaiTerendah
const nilaiTerendah = (values) => {
    var terendah = Math.min.apply(null, values);

    return terendah;
}
console.log(nilaiTerendah([50, 77, 31, 56, 88]));
//rata-rata
const rataRata = (values) => {
    const avg = values.reduce((a,b) => a + b)/values.length;
    return avg;
}

console.log(rataRata([90, 81, 56, 87, 76]));

//Jumlah siswa lulus
const jumlahLulus = (values) => {
    let jumlah = 0;
    values.forEach(function(nilai) {
        if(nilai >= 60) {
            jumlah++;
        }
    });
    return jumlahLulus;
}

console.log(jumlahLulus([60, 67, 40, 79, 90]));

//Jumlah siswa tidak Lulus
const tidakLulus = (values) => {
    let jumlah = 0;
    values.forEach(function(nilai) {
        if(nilai < 60) {
            jumlah++;
        }
    });
    return tidakLulus;
}

console.log(tidakLulus([56, 43, 79, 80, 90]));

//urutkan nilai siswa
